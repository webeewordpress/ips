
    $('#increasetext').click(function() {
		curSize = parseInt($('body').css('font-size')) + 2;
		if (curSize <= 32)
			$('body').css('font-size', curSize);
	});

	$('#resettext').click(function() {
		if (curSize != 16)
			$('body').css('font-size', 16);
	});

	$('#decreasetext').click(function() {
		curSize = parseInt($('body').css('font-size')) - 2;
		if (curSize >= 10)
			$('body').css('font-size', curSize);
	});
